'use strict';

var domain = require('../../domain');

var resultMaps = [
    {
        mapId: 'accountMap',
        createNew: function() {
            return new domain.Account();
        },
        properties: [
            {name: 'id', column: 'id'},
            {name: 'keystore', column: 'keystore'}
        ]
    }
];

module.exports = resultMaps;
