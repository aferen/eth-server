'use strict';

module.exports = {
    createAccount: createAccount,
    updateAccount: updateAccount,
    getAccount: getAccount,
    getAccountBalance: getAccountBalance,
    getAccounts: getAccounts,
    deleteAccount: deleteAccount
};

var knex = require('./db').knex;
var joinjs = require('join-js');
var resultMaps = require('./resultmaps');
var Account = require('../../domain').Account;
var config = require('../../../eth-server.config.js');
var web3 = require('web3');
//var web3 = new Web3();
var hookedWeb3Provider = require('hooked-web3-provider');
var BigNumber = require('bignumber.js');
var lightwallet = require('eth-lightwallet');
//var promise = require('bluebird');

var abi = [{'constant':true,'inputs':[],'name':'minter','outputs':[{'name':'','type':'address'}],'type':'function'},{'constant':true,'inputs':[{'name':'','type':'address'}],'name':'balances','outputs':[{'name':'','type':'uint256'}],'type':'function'},{'constant':false,'inputs':[{'name':'receiver','type':'address'},{'name':'amount','type':'uint256'}],'name':'mint','outputs':[],'type':'function'},{'constant':false,'inputs':[{'name':'receiver','type':'address'},{'name':'amount','type':'uint256'}],'name':'send','outputs':[],'type':'function'},{'constant':false,'inputs':[{'name':'addr','type':'address'}],'name':'getBalance','outputs':[{'name':'','type':'uint256'}],'type':'function'},{'inputs':[],'type':'constructor'},{'anonymous':false,'inputs':[{'indexed':false,'name':'from','type':'address'},{'indexed':false,'name':'to','type':'address'},{'indexed':false,'name':'amount','type':'uint256'}],'name':'Sent','type':'event'}]
var contractAddress = '0xf0b19ab8a87a8416ef845bdb84a6d35ff113d6aa';

var txutils = lightwallet.txutils;
var signing = lightwallet.signing;
var helpers = lightwallet.helpers;
var encryption = lightwallet.encryption;
var nonce = 1;

function setWeb3Provider(keystore) {
    var web3Provider = new hookedWeb3Provider({
        host: config.geth.host + ':' + config.geth.port,
        transaction_signer: keystore
    });
    // web3.provider = web3Provider;
    web3.setProvider(web3Provider);
}

lightwallet.keystore.prototype.passwordProvider = function (callback) {
  var password = "mypassword";
  callback(null, password);
}


/**
 * Creates a new account and inserts it in to the database.
 * @param {Object} accountData minus the id
 * @return {Promise} A promise that returns the inserted account (including the id)
 */
function createAccount(accountData) {
	console.log('accountData:' + accountData.password);

     var serializedKeystorePromise = new Promise(
    
    function(resolve, reject) {
    
	lightwallet.keystore.deriveKeyFromPassword('mypassword', function(err, pwDerivedKey) {

		var seed = lightwallet.keystore.generateRandomSeed();
   		var serializedKeystore = '';

		var keystore = new lightwallet.keystore(seed, pwDerivedKey)
		setWeb3Provider(keystore);

		keystore.generateNewAddress(pwDerivedKey)

		serializedKeystore = keystore.serialize();
		console.log('Created new keystore:' + serializedKeystore);

		var newAddress = keystore.getAddresses()[0];
		
		knex.insert({id: newAddress, keystore: serializedKeystore}).into('accounts')
		.catch(function(error) {
			console.error('Error occurred: ' + error);
			reject(error);
  		});
        
		resolve(newAddress);
	});
	
	}
	);
	
	return serializedKeystorePromise;
}

/**
 * Updates an existing account.
 * @param {Object} accountData including the id
 * @return {Promise} A promise that returns the updated account (including the id)
 */
function updateAccount(accountData) {

    var account = new Account(accountData);

    return knex('accounts')
        .where('id', account.id)
        .update(account)
        .then(function() {
            return account;
        });
}

/**
 * Gets an existing account.
 * @param {string} id
 * @return {Promise} A promise that returns the desired account.
 */
function getAccount(id) {
    return knex
        .select('id', 'keystore')
        .from('accounts')
        .where('id', id)

        .then(function(resultSet) {
            return joinjs.mapOne(resultSet, resultMaps, 'accountMap');
        });
}

function getAccountBalance(id) {
    return knex('accounts')
    	.pluck('keystore')
        .where('id', id)

        .then(function(serializedKeystores) {
 			
			var accountBalancePromise = new Promise(
    
    		function(resolve, reject) {
    		// stuck, so doing a hack here. extra cpu cycles, but no solution at the moment
    		var serializedKeystore = serializedKeystores[0];
			var keystore = lightwallet.keystore.deserialize(serializedKeystore);
			
			var fromAddress = keystore.getAddresses()[0];
			setWeb3Provider(keystore);
			var nonce =  web3.eth.getTransactionCount(fromAddress) + 1000;
			
			var txOptions = {
				gas : 61000, 
				gasPrice : 2100000000000,
	    		gasLimit: 3000000,
    			value: 10000,
    			nonce: nonce,
    			to: contractAddress
			};
			
			lightwallet.keystore.deriveKeyFromPassword('mypassword', function(err, pwDerivedKey) {
				var getBalanceTx = txutils.functionTx(abi, 'getBalance', ['0x' + id], txOptions);
				var signedTx = signing.signTx(keystore, pwDerivedKey, getBalanceTx, id)
			
				web3.eth.sendRawTransaction(signedTx.toString('hex'), function (err, hash) {
		    		if (err) {
        				reject(err)
    				}
    				else {
        				resolve(hash); 
    				}
				});
			});
			
			});
			
			return accountBalancePromise;
        });
}



/**
 * Gets all accounts.
 * @return {Promise} A promise that returns an array of all accounts.
 */
function getAccounts() {
    return knex
        .select('id', 'keystore')
        .from('accounts')

        .then(function(resultSet) {
            return joinjs.map(resultSet, resultMaps, 'accountMap');
        });
}

/**
 * Deletes an account.
 * @param {integer} id
 * @return {Promise} A promise that gets fulfilled when the account is deleted.
 */
function deleteAccount(id) {
    return knex('accounts')
        .where('id', id)
        .delete();
}
