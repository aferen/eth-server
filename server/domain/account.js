'use strict';

/**
 * An account.
 *   {String}  id
 *   {String}  name
 *
 * Example:
 *   {
 *       id: [number],
 *       name: 'Cash'
 *   }
 */

var _ = require('lodash');

var Account = function(accountData) {
    if (accountData) {
        _.extend(this, accountData);
    }
};

module.exports = Account;
