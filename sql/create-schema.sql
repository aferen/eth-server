-- Drop foreign keys
-- Example:
-- ALTER TABLE transactions
--   DROP CONSTRAINT transactions_account_id_foreign;

-- Drop tables
DROP TABLE IF EXISTS accounts;

-- Create tables
CREATE TABLE accounts (
  id VARCHAR(50) PRIMARY KEY,
  keystore VARCHAR(5000) NOT NULL
);

-- Add foreign keys
-- Example:
-- ALTER TABLE transactions
--   ADD COLUMN account_id INTEGER NOT NULL,
--   ADD COLUMN category_id INTEGER NOT NULL;
